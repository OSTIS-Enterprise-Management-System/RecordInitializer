﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ostis.Sctp;

namespace OstisAgent_RecordFill
{
    class Program
    {
        private const string IP = "192.168.38.134";

        static void Main(string[] args)
        {
            var recordFill = new RecordFill(IP, SctpProtocol.DefaultPortNumber,
                new ProductRecord(666, "Tasty White Milk"));
            
            recordFill.CreateRecordInstance();

            Console.ReadKey();
        }
    }
}
