﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using Ostis.Sctp;
using Ostis.Sctp.Arguments;
using Ostis.Sctp.Commands;
using Ostis.Sctp.Responses;

namespace OstisAgent_RecordFill
{
    public class RecordFill
    {
        private readonly SctpClient client;
        private readonly IRecord recordType;

        public Identifier RrelRecordField { get; } = "rrel_record_field";

        public Identifier RrelFirstRecordField { get; } = "rrel_first_record_field";

        public Identifier NrelNextItem { get; } = "nrel_next_item";

        public Identifier RecordTypeSysId => recordType.RecordTypeName;

        public Dictionary<Identifier, LinkContent> Fields { get; }


        public RecordFill(string address, int port, IRecord record)
        {
            client = new SctpClient(address, port);
            client.Connect();
            this.recordType = record;
            Fields = new Dictionary<Identifier, LinkContent>();
            foreach (var field in recordType.Fields)
            {
                Fields.Add(new Identifier(field.Key), new LinkContent(field.Value));
            }
        }

        private ScAddress CreateAndSetLinkCmd(LinkContent content)
        {
            var cmdCreateLink = new CreateLinkCommand();
            var rspCreateLink = (CreateLinkResponse) client.Send(cmdCreateLink);
            var cmdSetValue = new SetLinkContentCommand(rspCreateLink.CreatedLinkAddress, content);
            var rspSetValue = (SetLinkContentResponse) client.Send(cmdSetValue);
            return rspCreateLink.CreatedLinkAddress;
        }

        private ScAddress CreateNodeCmd(ElementType nodeType)
        {
            var cmdCreateNode = new CreateNodeCommand(nodeType);
            var rspCreateNode = (CreateNodeResponse) client.Send(cmdCreateNode);
            return rspCreateNode.CreatedNodeAddress;
        }

        private ScAddress CreateArcCmd(ElementType arcType, ScAddress beginElement, ScAddress endElement)
        {
            var cmdCreateArc = new CreateArcCommand(arcType, beginElement, endElement);
            var rspCreateArc = (CreateArcResponse)client.Send(cmdCreateArc);
            return rspCreateArc.CreatedArcAddress;
        }

        private ScAddress FindElementAddress(Identifier identifier)
        {
            var scaddress = ScAddress.Invalid;
            var cmdFindElement = new FindElementCommand(identifier);
            var rspFindElement = (FindElementResponse)client.Send(cmdFindElement);
            if (rspFindElement.Header.ReturnCode == ReturnCode.Successfull)
            {
                scaddress = rspFindElement.FoundAddress;
            }
            return scaddress;
        }

        private double FindValueByRelation(ScAddress node, ScAddress predicate)
        {
            var value = double.NaN;
            //итерируем конструкцию
            var template = new ConstructionTemplate(node, ElementType.ConstantCommonArc_c, ElementType.Link_a, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
            var cmdIterateElements = new IterateElementsCommand(template);
            var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

            //если число конструкций равно 1, то ищем значение ссылки

            if (rspIterateElements.Constructions.Count == 1)
            {
                var cmdGetValue = new GetLinkContentCommand(rspIterateElements.Constructions[0][2]);
                var rspGetValue = (GetLinkContentResponse)client.Send(cmdGetValue);
                value = LinkContent.ToDouble(rspGetValue.LinkContent);
            }
            return value;
        }

        public void CreateRecordInstance()
        {
            var rrelFieldAddr = FindElementAddress(RrelRecordField);
            var rrelFstFieldAddr = FindElementAddress(RrelFirstRecordField);
            var nrelNextItemAddr = FindElementAddress(NrelNextItem);

            var recordTypeAddr = FindElementAddress(RecordTypeSysId);
            var fieldAddrDict = new Dictionary<ScAddress, ScAddress>();
            foreach (var field in Fields)
            {
                var fieldNameAddr = FindElementAddress(field.Key);
                fieldAddrDict.Add(fieldNameAddr, CreateAndSetLinkCmd(field.Value));
            }

            foreach (var field in fieldAddrDict)
            {
                this.CreateArcCmd(ElementType.PositiveConstantPermanentAccessArc_c, field.Key, field.Value);
            }

            if (fieldAddrDict.Count > 1)
            {
                var names = fieldAddrDict.Keys;
                var values = fieldAddrDict.Values;

                var valuePairs = values.Zip(values.Skip(1), (Tuple.Create));
                foreach (var valuePair in valuePairs)
                {
                    var commmonArcAddr = this.CreateArcCmd(ElementType.ConstantCommonArc_c, valuePair.Item1,
                        valuePair.Item2);
                    this.CreateArcCmd(ElementType.PositiveConstantPermanentAccessArc_c, nrelNextItemAddr, commmonArcAddr);
                }
            }
        }

        //public bool FindRecordType(IRecord record)
        //{
        //    var recordScAddr = FindElementAddress(RecordTypeSysId);
        //    if (recordScAddr == null) return false;
        //    foreach (var field in Fields.Keys)
        //    {
        //        if (FindElementAddress(field) == null)
        //            return false;

        //    }
        //}
    }
}