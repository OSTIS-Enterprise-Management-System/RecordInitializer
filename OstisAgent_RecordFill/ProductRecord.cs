﻿using System.Collections.Generic;

namespace OstisAgent_RecordFill
{
    public class ProductRecord : IRecord
    {
        public string RecordTypeName => "product";

        public Dictionary<string, string> Fields => new Dictionary<string, string>
        {
            {"product_id", Id.ToString()},
            {"product_name", Name}
        };

        public int Id { get; set; }
        public string Name { get; set; }

        public ProductRecord(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}