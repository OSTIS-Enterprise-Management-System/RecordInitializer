﻿using System;
using System.Collections.Generic;

namespace OstisAgent_RecordFill
{
    public interface IRecord
    {
        string RecordTypeName { get; }
        Dictionary<string, string> Fields { get; }
    }
}